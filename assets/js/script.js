'use strict';

$(function () {

    $(window).on("load",function () {
        $("#loading").delay(500).fadeOut(0);
        setTimeout(function() {
            $('body').removeClass('stop-scrolling');
        }, 500);
    });

    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
            document.getElementById("navbar").style.padding = "5px 20px";
            document.getElementById("return-to-top").style.display = "block";
        } else {
            document.getElementById("navbar").style.padding = "20px";
            document.getElementById("return-to-top").style.display = "none";
        }
    }

    $('a[href^="#"]').click(function(event) {
        var id = $(this).attr("href");
        var offset = 60;
        var target = $(id).offset().top - offset;
        $('html, body').animate({scrollTop:target}, 1500);
        event.preventDefault();
    });

    $('#return-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 1500);
    });

    var swiper = new Swiper('.swiper-container', {
        autoplay: {
            delay: 2000
        },
        loop: true,
        speed: 2000,
        slidesPerView: 'auto',
        grabCursor: true,
        effect: 'fade',
    });

    $('.swiper-container').on('mouseenter', function(){
        swiper.autoplay.stop();
    }).on('mouseleave', function() {
        swiper.autoplay.start();
    });

    lightbox.option({
        showImageNumberLabel: false,
        wrapAround: true,
        positionFromTop: 0,
        disableScrolling: true
    });

    $('.slider-box').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true
    });

});

